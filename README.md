# Сборки

Здесь лежат актуальные прошивки. 

```
0x0000 - 0x06800 - Bootloader
0x6800 - 0x3FFFF - ProjM (BLE & Application) 
```

Актуальная версия приложения размещена в папке 
https://bitbucket.org/neurotreker/neuro_public/downloads/


# Версии 

## Версия 1.1.76

Исправлено, что перестала работать загрузка обновления. 
